import { createServer, Model } from 'miragejs';
import { sections, posts, versions } from './models';

export function makeServer ({ environment = 'development' } = {}) {
  const server = createServer({
    environment,

    models: {
      section: Model,
      Post: Model,
      version: Model
    },

    seeds (server) {
      sections.forEach(s => server.create('section', s));
      posts.forEach(s => server.create('post', s));
      versions.forEach(s => server.create('version', s));

    },

    routes () {
      this.namespace = 'api/v1';

      // Offsets & limits don't need to be implemented here, as the data set isn't large enough to require this yet
      this.get('/sections', (schema) => {
        return schema.sections.all();
      })
      this.get('/posts', (schema) => {
        return schema.posts.all();
      })
      this.get('/versions', (schema) => {
        return schema.versions.all();
      })
    }
  });

  return server;
}
