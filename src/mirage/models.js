export const sections = [
  {
    id: '837ba6a5-47ed-4682-bff5-90bc5c5f9646',
    title: 'Section One',
    shortname: 'one',
    description: 'The first section.',
    is_default: true,
    has_titles: true,
    is_private: false,
    is_archived: false
  },
  {
    id: '5e661b3f-61e6-4aed-a93b-8cae0896f656',
    title: 'Section Two',
    shortname: 'two',
    description: 'The second section.',
    is_default: true,
    has_titles: false,
    is_private: false,
    is_archived: false
  },
  {
    id: '5fcbeaca-1496-438e-ace2-18e99e11f384',
    title: 'Section Three',
    shortname: 'three',
    description: 'The third section.',
    is_default: false,
    has_titles: true,
    is_private: false,
    is_archived: false
  }
];

export const posts = [
  {
    id: 'af08bbcd-f6eb-446e-b355-13e0a0ef008e',
    section_id: sections[0]['id'],
    is_private: false,
    is_archived: false
  }
];

export const versions = [
  {
    id: '8c5bc2f9-e52f-4e19-bd76-119cc42ff863',
    post_id: posts[0]['id'],
    title: 'This Is A Title',
    publish_date: '2021-12-28',
    content: 'Hello. This is some content!',
    is_draft: false
  }
]
