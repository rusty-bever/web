export interface Section {
  id: str,
  shortname: str,
  description: str,
  is_default: bool,
  has_titles: bool,
  is_private: bool,
  is_archived: bool
}

export interface Post {
  id: str,
  section_id: str,
  is_private: bool,
  is_archived: bool
}

export interface Version {
  id: str,
  post_id: str,
  title: str,
  publish_date: Date,
  content: str,
  is_draft: bool
}
