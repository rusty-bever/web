import { createApp } from 'vue'
import App from './App.vue'
// @ts-ignore
import { makeServer } from './mirage/v1'

if (import.meta.env.DEV) {
  makeServer()
}

createApp(App).mount('#app')
