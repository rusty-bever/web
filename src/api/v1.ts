const API_PREFIX = "/api/v1"

export class RbApi {
  async get_json(path) {
    const url = `${API_PREFIX}${path}`
    let res = await fetch(url)
    return await res.json()
  }

  async sections() {
    let res = await this.get_json("/sections")

    return res["sections"]
  }
}
