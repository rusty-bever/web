# ======Building the project=====
FROM node:17.3.0 AS builder

WORKDIR /usr/src/app

# Install Node dependencies
COPY package.json yarn.lock ./
RUN yarn install

# Copy over source code & build project
COPY vite.config.ts tsconfig.json index.html ./
COPY src/ ./src
COPY public/ ./public
RUN yarn run build


# =====Packaging inside an Nginx container=====
FROM nginx:1.21.5-alpine

# Copy over the Nginx config files
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/*.conf.template /etc/nginx/templates/
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# Copy over build artifacts
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html
